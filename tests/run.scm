(import chicken.file chicken.format test)
(import (prefix gnuplot-pipe gp:))

;;; Evaluate gp:call/gnuplot setting a non-persistent terminal, save
;;; the (last) plot to file, test-assert if the file exists and delete
;;; it.
(define-syntax call/gnuplot-test
  (syntax-rules ()
    ((_ expr ...)
     (let ((fname "test.png"))
       (gp:call/gnuplot
        (gp:send "set term x11 nopersist")
        expr ...
        (gp:save fname))
       (test-assert (file-exists? fname))
       (delete-file fname)))))

;;; Plot function.
(call/gnuplot-test
 (gp:send "set xlabel 'x'")
 (gp:send "plot sin(x)"))

;;; Plot x and y axes data as lines.
(call/gnuplot-test
 (gp:plot "title 'x^2'" '(1 2 3 4) '(1 4 9 16)))

;;; Plot multiple curves.
(call/gnuplot-test
 (gp:plot '(("title 'x^2'" (1 2 3 4) (1 4 9 16)))))

(call/gnuplot-test
 (gp:plot '(("title 'x^2'" (1 2 3 4) (1 4 9 16))
            ("title 'x^3'" (1 2 3 4) (1 8 27 64)))))

(call/gnuplot-test
 (gp:plot '(("title 'x'" (1 2 3 4) (1 2 3 4))
            ("title 'x^2'" (1 2 3 4) (1 4 9 16))
            ("title 'x^3'" (1 2 3 4) (1 8 27 64)))))

;;; 3d plot.
(call/gnuplot-test
 (gp:send "unset key")
 (gp:send "set style data points")
 (gp:send "set title 'The valley of the Gnu'")
 (gp:plot3d ""
            '(0 0 0 1 1 1 2 2 2 3 3 3)
            '(0 1 2 0 1 2 0 1 2 0 1 2)
            '(10 10 10 10 5 10 10 1 10 10 0 10)))

(call/gnuplot-test
 (gp:send "unset key")
 (gp:send "set style data points")
 (gp:send "set title 'The valley of the Gnu'")
 (gp:plot3d '((""
               (0 0 0 1 1 1 2 2 2 3 3 3)
               (0 1 2 0 1 2 0 1 2 0 1 2)
               (10 10 10 10 5 10 10 1 10 10 0 10)))))

(call/gnuplot-test
 (gp:send "set style data points")
 (gp:send "set title 'The valley of the Gnu'")
 (gp:plot3d '(("title 'series 1'"
               (0 0 0 1 1 1 2 2 2 3 3 3)
               (0 1 2 0 1 2 0 1 2 0 1 2)
               (10 10 10 10 5 10 10 1 10 10 0 10))
              ("title 'series 2'"
               (0 0 0 1 1 1 2 2 2 3 3 3)
               (0 1 2 0 1 2 0 1 2 0 1 2)
               (10 10 10 10 5 10 10 1 10 10 0 10)))))

;;; Pie chart.
(call/gnuplot-test
 (gp:send "set xrange [-15:15]")
 (gp:send "set style fill transparent solid 0.9 noborder")
 (gp:plot "using 1:2:3:4:5:6 with circles lc var"
          '(0 0 0 0 0) '(0 0 0 0 0) '(5 5 5 5 5)
          '(0 30 70 120 230) '(30 70 120 230 360) '(1 2 3 4 5)))

;;; Matrix
(call/gnuplot-test
 (gp:plot "matrix with image"
          '(5 2 0 0) '(4 2 0 1) '(3 0 0 2) '(1 0 1 4) '(0 1 0 3))
 (gp:end-data))

;;; Dates
(call/gnuplot-test
 (gp:send "set xdata time")
 (gp:send "set timefmt \"%m/%d/%y\"")
 (gp:send "set xrange [\"03/21/95\":\"03/22/95\"]")
 (gp:send "set format x \"%m/%d\"")
 (gp:send "set timefmt \"%m/%d/%y %H:%M\"")
 (gp:plot "using 1:3 with points"
          '("03/21/95") '("10:00") '(6.02e23)))

(test-exit)
